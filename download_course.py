import os, sys, re, time, traceback
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

import requests
import urllib.request
import time
from bs4 import BeautifulSoup

import threading

# import Colorama
try:
    from colorama import init

    init()

    from colorama import Fore, Back, Style

except ImportError:
    print('Colorama not imported')

dirapp = os.getcwd()


def remove_suffix(text, suffix):
   if text.endswith(suffix):
       return text[:len(text)-len(suffix)]
   return text #or whatever


def download_video(file_url, filename=None, location=None):
    if filename is None:
        filename = os.path.basename(file_url).replace('%20', ' ').replace('%21', '!')

    # 1
    if location:
        if os.sep == '\\':  # if windows
            location = location.split(os.sep)
            drive, location = location[:1][0].strip(':'), location[1:]
            location = os.path.join('mnt', drive, *location)
            location = '/' + location.replace("\\", "/")

        cmd = 'bash -c "wget {} -P \'{}\'"'.format(file_url, location)
        # os.system(cmd)
        print(cmd)
    else:
        os.system('bash -c "wget {}"'.format(file_url))
# 2
    return
    r = requests.get(file_url, stream=True)
    with open(filename, "wb") as video:
        for chunk in r.iter_content(chunk_size=1024):
            # writing one chunk at a time to video file
            if chunk:
                video.write(chunk)

    print('File "{}" saved.'.format(filename))


url = 'https://coursevania.courses.workers.dev/[coursevania.com]%20Udemy%20-%20Monster%20JavaScript%20Course%20-%2050+%20projects%20and%20applications/'
url = 'https://coursevania.courses.workers.dev/[coursevania.com]Python%20for%20Data%20Science%20and%20Machine%20Learning%20Bootcamp/'
mode = 2
# driver = webdriver.Chrome()
driver = webdriver.Chrome(ChromeDriverManager().install())
driver.get(url)
driver.maximize_window()

#username_box = driver.find_element_by_id('login_username')
#username_box.send_keys(user)
# login_button.click()

if mode == 1:  # all videos in the root directory
    link_elements_list = driver.find_elements_by_css_selector('.file [href]')  # or: driver.find_elements(By.CSS_SELECTOR, '.theClass')
    links = [elem.get_attribute('href') for elem in link_elements_list]

    thread_list = []
    lesson_list = {}
    limit = False
    count = 0

    for l in links:
        count += 1
        if limit:
            if count > limit:
                break

        driver.get(l)
        try:
            time.sleep(1)
            video_url = driver.find_elements_by_css_selector('.mdui-textfield-input')[0].get_attribute('value')
        except:
            print('{color}{url}{reset}'.format(color=Fore.RED + Style.BRIGHT, url=l,
                                               reset=Style.RESET_ALL))  # TODO : add to a list which needs to be printed a the end
            continue

        video_n_regex = re.compile(r'Lesson%20(\d{1,3})')
        video_n = video_n_regex.findall(video_url)
        if video_n:
            video_n = int(video_n[0])
            lesson_list[video_n] = video_url

        t = threading.Thread(target=download_video, args=(video_url,))
        thread_list.append(t)
        t.start()

    for t in thread_list:
        t.join()
        pass

    print('\n\n')
    for lesson_n in sorted(lesson_list.keys()):
        print('{color}Lesson {n}{reset}'.format(color=Fore.CYAN + Style.BRIGHT, n=lesson_n, reset=Style.RESET_ALL))


elif mode == 2:  # videos in the subfolders
    folder_links = {}
    video_links = {}
    link_elements_list = driver.find_elements_by_css_selector('.mdui-list-item [href]')  # or: driver.find_elements(By.CSS_SELECTOR, '.theClass')
    # links = elem.get_attribute('href') for elem in link_elements_list]
    for elem in link_elements_list:
        name_regex = re.compile(r'(\d+\..*)[\r\n]')
        f_name = name_regex.findall(elem.text)[0]
        f_link = elem.get_attribute('href')

        if f_name not in video_links:
            video_links[f_name] = []

        if f_name not in folder_links:
            folder_links[f_name] = []
        folder_links[f_name].append(f_link)

    for f_name, f_link_list in folder_links.items():
        for f_link in f_link_list:
            driver.get(f_link)
            try:
                time.sleep(2)
                video_urls = driver.find_elements_by_css_selector('.mdui-list-item [href]')
                video_urls = [remove_suffix(elem.get_attribute('href'), '?a=view') for elem in video_urls]
                video_links[f_name] += video_urls
            except:
                traceback.print_exc()
                print('{color}{url}{reset}'.format(color=Fore.RED + Style.BRIGHT, url=f_link, reset=Style.RESET_ALL))  # TODO : add to a list which needs to be printed a the end
                continue

            for folder, links in video_links.items():
                folder_fullpath = os.path.realpath(folder)
                if not os.path.exists(folder):
                    os.makedirs(folder)

                for l in links:
                    # t = threading.Thread(target=download_video, args=(l, None, folder_fullpath))
                    # t.start()
                    pass
                    download_video(l, None, folder_fullpath)

                try:
                    t.join()
                except:
                    pass
            print('')
        print('')


print('Done')

"""
import requests
import urllib.request
import time
from bs4 import BeautifulSoup


response = requests.get(url)

soup = BeautifulSoup(response.text, "html.parser")
video_links = soup.findAll('a')


print('Done')
"""

# TODO: limit the number of concurrent downloads
# TODO: create a mode (add argparse) for only displaying the links, so that they can be downloaded by a download manager
